import { CanvasComponent } from './canvas/canvas.component';
import { DialogComponent } from './dialog/dialog.component';

export {
  CanvasComponent,
  DialogComponent
};

export default [
  CanvasComponent,
  DialogComponent
];
