import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { map, switchMap, takeUntil, pairwise, merge } from 'rxjs/operators';
import { DialogData } from 'src/app/models/dialog';

const mouseEventToCoordinate = mouseEvent => {
  mouseEvent.preventDefault();
  return { x: mouseEvent.clientX, y: mouseEvent.clientY };
};
const touchEventToCoordinate = touchEvent => {
  touchEvent.preventDefault();
  return { x: touchEvent.changedTouches[0].clientX, y: touchEvent.changedTouches[0].clientY };
};
const x_axis_starting_point = { number: 0.5, suffix: '' };
const y_axis_starting_point = { number: 2, suffix: '' };

@Component({
  selector: 'app-canvas',
  template: '<canvas #canvas></canvas>',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas') public canvas: ElementRef;
  private canvasEl: HTMLCanvasElement;
  private cx: CanvasRenderingContext2D;
  private PI: number = Math.PI;
  para: DialogData = { a: 1, b: 0, c: 0, d: 0.5, e: 2 };
  grid_size = 25;
  width = 400;
  height = 400;
  x_axis_offset = 250;
  y_axis_offset = 100;
  mouseDown; mouseMove; mouseUp;
  touchStart; touchMove; touchEnd;
  start; move; end;
  moveX: number;
  moveY: number;

  X(x) { return this.grid_size * x / x_axis_starting_point.number; }
  Y(y) { return -this.grid_size * y / y_axis_starting_point.number; }

  constructor() { }
  ngOnInit() { }
  ngAfterViewInit() {
    this.canvasEl = this.canvas.nativeElement;
    this.cx = this.canvasEl.getContext('2d');
    this.x_axis_offset = this.grid_size * Math.floor(2 * this.cx.canvas.offsetHeight / 3 / this.grid_size);
    this.grid();
    this.captureEvents();
    this.captureMoves();
  }

  func(x): number { return this.para.a * Math.pow(x, 2) + this.para.b * x + this.para.c; }
  line(x): number { return this.para.d * x + this.para.e; }

  drawFunc() {
    this.cx.strokeStyle = 'red';
    this.cx.beginPath();
    let y = 0;
    for (let x = -Math.floor(x_axis_starting_point.number * this.y_axis_offset / this.grid_size) - 2;
      x < Math.ceil(this.width / this.grid_size);) {
      this.cx.moveTo(this.X(x), this.Y(y));
      x += x_axis_starting_point.number;
      y = this.func(x);
      this.cx.lineTo(this.X(x), this.Y(y));
      this.cx.stroke();
    }
  }
  drawLine() {
    this.cx.strokeStyle = 'green';
    this.cx.beginPath();
    let y = 0;
    for (let x = -Math.floor(x_axis_starting_point.number * this.y_axis_offset / this.grid_size) - 2;
      x < Math.ceil(this.width / this.grid_size);) {
      this.cx.moveTo(this.X(x), this.Y(y));
      x += x_axis_starting_point.number;
      y = this.line(x);
      this.cx.lineTo(this.X(x), this.Y(y));
      this.cx.stroke();
    }
  }

  captureMoves() {
    this.mouseDown = fromEvent(this.canvasEl, 'mousedown').pipe(map(mouseEventToCoordinate));
    this.mouseMove = fromEvent(this.canvasEl, 'mousemove').pipe(map(mouseEventToCoordinate));
    this.mouseUp = fromEvent(this.canvasEl, 'mouseup').pipe(map(mouseEventToCoordinate));

    this.touchStart = fromEvent(this.canvasEl, 'touchstart').pipe(map(touchEventToCoordinate));
    this.touchMove = fromEvent(this.canvasEl, 'touchmove').pipe(map(touchEventToCoordinate));
    this.touchEnd = fromEvent(this.canvasEl, 'touchend').pipe(map(touchEventToCoordinate));

    this.start = this.mouseDown.pipe(merge(this.touchStart));
    this.move = this.mouseMove.pipe(merge(this.touchMove));
    this.end = this.mouseUp.pipe(merge(this.touchEnd));

    this.start.forEach(c => {
      const moveX = c.x;
      const moveY = c.y;
      const oX = this.y_axis_offset;
      const oY = this.x_axis_offset;
      this.move.pipe(takeUntil(this.end)).subscribe((ev) => {
        this.x_axis_offset = oY + ev.y - moveY;
        this.y_axis_offset = oX + ev.x - moveX;
        this.grid();
      });
    });
    // this.end.forEach(c => {
    //   const diffX = this.moveX - c.x;
    //   const diffY = this.moveY - c.y;
    //   console.log('Diff: ', diffX, diffY);
    //   this.x_axis_offset -= diffY;
    //   this.y_axis_offset -= diffX;
    //   this.grid();
    // });
  }

  grid(para?: DialogData) {
    if (para) { this.para = para; }
    this.width = this.cx.canvas.offsetWidth;
    this.height = this.cx.canvas.offsetHeight;
    this.canvasEl.width = this.width;
    this.canvasEl.height = this.height;

    const num_lines_x = Math.floor(this.height / this.grid_size);
    const num_lines_y = Math.floor(this.width / this.grid_size);

    this.cx.lineWidth = 1;
    this.cx.font = '9px Arial';

    const xoff = this.x_axis_offset % this.grid_size;
    const yoff = this.y_axis_offset % this.grid_size;

    for (let i = 0; i <= num_lines_x; i++) {
      this.cx.beginPath();
      // If line represents X-axis draw in different color
      if (i === Math.floor(this.x_axis_offset / this.grid_size)) {
        this.cx.strokeStyle = '#000000';
      } else {
        this.cx.strokeStyle = '#e9e9e9';
      }
      if (i === num_lines_x) {
        this.cx.moveTo(0, this.grid_size * i + xoff);
        this.cx.lineTo(this.width, this.grid_size * i + xoff);
      } else {
        this.cx.moveTo(0, this.grid_size * i + 0.5 + xoff);
        this.cx.lineTo(this.width, this.grid_size * i + 0.5 + xoff);
      }
      this.cx.stroke();
    }

    for (let i = 0; i <= num_lines_y; i++) {
      this.cx.beginPath();
      // If line represents Y-axis draw in different color
      if (i === Math.floor(this.y_axis_offset / this.grid_size)) {
        this.cx.strokeStyle = '#000000';
      } else {
        this.cx.strokeStyle = '#e9e9e9';
      }
      if (i === num_lines_y) {
        this.cx.moveTo(this.grid_size * i + yoff, 0);
        this.cx.lineTo(this.grid_size * i + yoff, this.height);
      } else {
        this.cx.moveTo(this.grid_size * i + 0.5 + yoff, 0);
        this.cx.lineTo(this.grid_size * i + 0.5 + yoff, this.height);
      }
      this.cx.stroke();
    }

    this.cx.translate(this.y_axis_offset, this.x_axis_offset);

    this.cx.strokeStyle = '#000000';
    for (let i = 1; i < (num_lines_y - Math.floor(this.y_axis_offset / this.grid_size)); i++) {
      this.cx.beginPath();
      // Draw a tick mark 6px long (-3 to 3)
      this.cx.moveTo(this.grid_size * i + 0.5, -3);
      this.cx.lineTo(this.grid_size * i + 0.5, 3);
      this.cx.stroke();
      // Text value at that point
      this.cx.textAlign = 'start';
      this.cx.fillText(x_axis_starting_point.number * i + x_axis_starting_point.suffix, this.grid_size * i - 2, 15);
    }
    for (let i = 1; i < Math.floor(this.y_axis_offset / this.grid_size); i++) {
      this.cx.beginPath();
      // Draw a tick mark 6px long (-3 to 3)
      this.cx.moveTo(-this.grid_size * i + 0.5, -3);
      this.cx.lineTo(-this.grid_size * i + 0.5, 3);
      this.cx.stroke();
      // Text value at that point
      this.cx.textAlign = 'end';
      this.cx.fillText(-x_axis_starting_point.number * i + x_axis_starting_point.suffix, -this.grid_size * i + 3, 15);
    }

    for (let i = 1; i < (num_lines_x - Math.floor(this.x_axis_offset / this.grid_size)); i++) {
      this.cx.beginPath();
      // Draw a tick mark 6px long (-3 to 3)
      this.cx.moveTo(-3, this.grid_size * i + 0.5);
      this.cx.lineTo(3, this.grid_size * i + 0.5);
      this.cx.stroke();
      // Text value at that point
      this.cx.textAlign = 'start';
      this.cx.fillText(-y_axis_starting_point.number * i + y_axis_starting_point.suffix, 8, this.grid_size * i + 3);
    }
    for (let i = 1; i < Math.floor(this.x_axis_offset / this.grid_size); i++) {
      this.cx.beginPath();
      // Draw a tick mark 6px long (-3 to 3)
      this.cx.moveTo(-3, -this.grid_size * i + 0.5);
      this.cx.lineTo(3, -this.grid_size * i + 0.5);
      this.cx.stroke();
      // Text value at that point
      this.cx.fillText(y_axis_starting_point.number * i + y_axis_starting_point.suffix, 8, -this.grid_size * i + 3);
    }
    this.drawFunc();
    this.drawLine();
  }

  private captureEvents() {
    const obs = fromEvent(this.canvasEl, 'mousedown').pipe(
      switchMap((e) => {
        return fromEvent(this.canvasEl, 'mousemove').pipe(
          takeUntil(fromEvent(this.canvasEl, 'mouseup')),
          takeUntil(fromEvent(this.canvasEl, 'mouseleave')),
          pairwise()
        );
      })
    ).subscribe((res: [MouseEvent, MouseEvent]) => {
      const rect = this.canvasEl.getBoundingClientRect();
      const prevPos = { x: res[0].clientX - rect.left, y: res[0].clientY - rect.top };
      const currentPos = { x: res[1].clientX - rect.left, y: res[1].clientY - rect.top };
      // do stuff while moving..
    });
  }
}
