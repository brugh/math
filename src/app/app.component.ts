import { Component, HostListener, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent, CanvasComponent } from './components';
import { DialogData } from './models/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  @ViewChild(CanvasComponent) cx: CanvasComponent;
  data: DialogData = { a: 1, b: 0, c: 0, d: 0.5, e: 2 };

  @HostListener('window:resize', ['$event']) onresize(ev) { this.cx.grid(); }

  constructor(public dialog: MatDialog) {}
  ngAfterViewInit() { this.cx.grid(); }

  open() {
    const ref = this.dialog.open(DialogComponent, { data: this.data });
    ref.afterClosed().subscribe(result => {
      if (result) {
        this.cx.grid(<DialogData>result); }
    });
  }
}
